package org.sber;

import org.junit.Before;
import org.junit.Test;


import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

public class CountMapImplTest {

   CountMapImpl<Integer> countMap;

    @Before
    public void setUp() {
        countMap = new CountMapImpl<>();
        countMap.add(5);
        countMap.add(5);
        countMap.add(5);

        countMap.add(3);
        countMap.add(3);

        countMap.add(10);

    }

    @Test
    public void getCountAndRemove() {
        assertEquals(countMap.getCount(5), 3);
        assertEquals(countMap.getCount(3), 2);
        assertEquals(countMap.getCount(10), 1);

        assertEquals( countMap.remove(5), 3);
    }

    @Test
    public void addAll(){
        CountMap<Integer> countMap2 = new CountMapImpl<>();
        countMap2.add(3);
        countMap2.add(10);
        countMap2.add(15);

        countMap.addAll(countMap2);

        assertEquals(countMap.getCount(3), 3);
        assertEquals(countMap.getCount(10), 2);
        assertEquals(countMap.getCount(15), 1);
    }

    @Test
    public void toMap(){
        Map<Integer, Integer> map = countMap.toMap();
        assertTrue(map.containsKey(5));
        assertTrue(map.containsValue(3));

        assertTrue(map.containsKey(3));
        assertTrue(map.containsValue(2));

        assertTrue(map.containsKey(10));
        assertTrue(map.containsValue(1));
    }

    @Test
    public void toMapDestination(){
        Map<Integer, Integer> map = new HashMap<Integer, Integer>();
        countMap.toMap(map);

        assertTrue(map.containsKey(5));
        assertTrue(map.containsValue(3));

        assertTrue(map.containsKey(3));
        assertTrue(map.containsValue(2));

        assertTrue(map.containsKey(10));
        assertTrue(map.containsValue(1));
    }

}