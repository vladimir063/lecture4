package org.sber;


import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;


public class CountMapImpl<V> implements CountMap<V> {

    private final Set<Node<V>> container = new HashSet<>();

    @Override
    public int getCount(V value) {
        Node<V> node = new NodeImpl<>(value);
        if (container.contains(node)) {
            Node<V> searchNode = searchNode(node);
            return searchNode.getCount();
        }
        return 0;
    }

    @Override
    public void add(V value) {
        Node<V> node = new NodeImpl<>(value);
        if (container.contains(node)) {
            Node<V> searchNode = searchNode(node);
            int count = searchNode.getCount();
            searchNode.setCount(++count);
        } else {
            container.add(node);
        }
    }

    @Override
    public int remove(V value) {
        Node<V> node = new NodeImpl<>(value);
        if (container.contains(node)) {
            Node<V> searchNode = searchNode(node);
            container.remove(searchNode);
            return searchNode.getCount();
        }
        return 0;
    }

    @Override
    public int size() {
        return container.size();
    }

    @Override
    public void addAll(CountMap<V> source) {
        source.getContainer().forEach(
                node -> {
                    if (container.contains(node)) {
                        Node<V> searchNode = searchNode(node);
                        int count = searchNode.getCount() + node.getCount();
                        searchNode.setCount(count);
                    } else {
                        container.add(node);
                    }
                });
    }

    @Override
    public Map<V, Integer> toMap() {
        return container.stream().collect(Collectors.toMap(Node::getValue, Node::getCount));
    }

    @Override
    public void toMap(Map<V, Integer> destination) {
        destination.putAll(toMap());
    }

    private Node<V> searchNode(Node<V> node) {
        return container.stream().filter(n -> n.equals(node)).findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }

    public Set<Node<V>> getContainer() {
        return container;
    }


    private static final class NodeImpl<V>  implements Node<V>{
        private final V value;
        private int count = 1;

        public NodeImpl(V value) {
            this.value = value;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Node<V> node = (Node<V>) o;
            return Objects.equals(value, node.getValue());
        }

        @Override
        public int hashCode() {
            return Objects.hash(value);
        }

        public V getValue() {
            return value;
        }

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }
    }
}
