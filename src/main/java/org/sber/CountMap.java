package org.sber;

import java.util.Map;
import java.util.Set;

public interface CountMap<V> {
    // добавляет элемент в этот контейнер
    void add(V value);

    //Возвращает количество добавлений данного элемента
    int getCount(V value);

    //Удаляет элемент и контейнера и возвращает количество его добавлений(до удаления)
    int remove(V value);

    //Добавить все элементы из source в текущий контейнер, при совпадении ключей, суммировать значения
    void addAll(CountMap<V> source);

    //количество разных элементов
    int size();

    //Вернуть java.util.Map. ключ - добавленный элемент, значение - количество его добавлений
    Map<V, Integer> toMap();

    //Тот же самый контракт как и toMap(), только всю информацию записать в destination
    void toMap(Map<V, Integer> destination);

    public Set<Node<V>> getContainer();

    interface Node<V> {

        V getValue();

        int getCount();

        void setCount(int count);
    }

}
